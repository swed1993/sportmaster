<?php
namespace App\Models;

use Dingo\Blueprint\Annotation\Parameters;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table = 'tasks';


    public function categories()
    {
        return $this->belongsTo(Category::class,'category_id');
    }

    public function levels()
    {
        return $this->belongsTo(Level::class,'level_id');
    }

//    public function parameters_tasks()
//    {
//        return $this->hasMany(Parameters::class);
//    }

    public function serial_number()
    {
        return $this->hasOne(SerialNumber::class);
    }

    public function realization()
    {
        return $this->hasMany(Realization::class);
    }
}
?>