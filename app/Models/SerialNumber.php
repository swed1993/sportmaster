<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SerialNumber extends Model
{
    protected $table = 'serial_number';
    public $timestamps = false;

    public function categories()
    {
        return $this->belongsTo(Category::class);
    }

    public function tasks()
    {
        return $this->belongsTo(Task::class);
    }
}
?>