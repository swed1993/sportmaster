<?php
namespace App\Services;


use App\Models\Category;
use App\Models\Client;
use App\Models\Level;
use App\Models\Parameter;
use App\Models\Realization;
use App\Models\Task;
use Illuminate\Support\Facades\Log;

class OtherService
{
    public static function create_category($request)
    {
        $model = new Category();
        $pocket = Category::where('name',$request['name'])->get();
        if(!empty($pocket))
            redirect('/create_task');
        $model->name = $request['name'];
        $model->prize = $request['prize'];
        $model->save() ? : Log::error('Ошибка создания категории');
    }

    public static function create_level($request)
    {
        $model = new Level();
        $pocket = Level::where('value',$request['name'])->get();
        if(!empty($pocket))
            redirect('/create_task');
        $model->value = $request['name'];
        $model->save() ? : Log::error('Ошибка создания уровня');
    }

    public static function create_status($request)
    {
        dd($request);
    }

    

}
?>