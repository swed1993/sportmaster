<?php
Route::group(['middleware' => ['auth']], function (){

	//Админ
	Route::get('logout',['as' => 'logout', 'uses' => '\App\Http\Controllers\Auth\AuthController@logout']);
	Route::get('',['as' => 'list_task', 'uses' => '\App\Http\Controllers\TaskController@list_task']);

	//Задания
	Route::get('create_task', ['as' => 'create_task', 'uses' => '\App\Http\Controllers\TaskController@index']);
	Route::post('task', ['as' => 'test', 'uses' => '\App\Http\Controllers\TaskController@task']);
	Route::get('list_task', ['as' => 'list_task', 'uses' => '\App\Http\Controllers\TaskController@list_task']);
	Route::post('edit_task', ['as' => 'edit_task', 'uses' => '\App\Http\Controllers\TaskController@edit_task']);
	Route::delete('/task/{task}','\App\Http\Controllers\TaskController@destroy');
	Route::post('/task/{task}','\App\Http\Controllers\TaskController@task_detail');
	
	
	//Клиенты
	Route::get('create_client', ['as' => 'create_client' , 'uses' => '\App\Http\Controllers\ClientController@create_client']);
	Route::get('list_clients', ['as' => 'list_clients' , 'uses' => '\App\Http\Controllers\ClientController@list_clients']);
	Route::post('client', ['as' => 'client' , 'uses' => '\App\Http\Controllers\ClientController@client']);


	//Категории
	Route::get('category', ['as' => 'category' , 'uses' => '\App\Http\Controllers\OtherController@category']);
	Route::post('create_category', ['as' => 'create_category' , 'uses' => '\App\Http\Controllers\OtherController@create_category']);
	Route::get('list_category', ['as' => 'list_category' , 'uses' => '\App\Http\Controllers\OtherController@list_category']);
	Route::delete('/category/{category}','\App\Http\Controllers\OtherController@category_destroy');
	Route::post('/category/{category}','\App\Http\Controllers\OtherController@category_detail');
	Route::post('/save_category','\App\Http\Controllers\OtherController@save_category');


	//Уровень
	Route::get('level', ['as' => 'level' , 'uses' => '\App\Http\Controllers\OtherController@level']);
	Route::post('create_level', ['as' => 'create_level' , 'uses' => '\App\Http\Controllers\OtherController@create_level']);
	Route::get('list_levels', ['as' => 'list_levels' , 'uses' => '\App\Http\Controllers\OtherController@list_levels']);
	Route::delete('/level/{level}','\App\Http\Controllers\OtherController@level_destroy');
	Route::post('/level/{level}','\App\Http\Controllers\OtherController@level_detail');
	Route::post('/save_level','\App\Http\Controllers\OtherController@save_level');


	Route::get('status', ['as' => 'status' , 'uses' => '\App\Http\Controllers\OtherController@status']);
	Route::post('create_status', ['as' => 'create_status' , 'uses' => '\App\Http\Controllers\OtherController@create_status']);




//	Route::get('list_task', ['as' => 'list_task', function () {
//		$content = 'Define your information here.';
//		return AdminSection::view($content, 'list_task');
//	}]);
	

});


