<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    protected $table = 'parameters_tasks';
    public $timestamps = false;

    public function tasks()
    {
        return $this->belongsTo(Task::class);
    }
}
?>