<div>
    <form class="form-horizontal" role="form" method="POST" action="{{ url('/client') }}" enctype="multipart/form-data">
        {{ csrf_field() }}


        <div class="no-padding">
            Телефон<input type="text" name="number" value=""><br>
            <label for="name_cat" class="col-md-4 control-label">Выберите задание</label>
            <div class="col-md-6">
                <select class="form-control" name="task">
                    @foreach($tasks as $key => $value)
                        <option value="{{ $value['name'] }}">{{ $value['name'] }}</option>
                    @endforeach
                </select>
            </div>
            <select class="form-control" name="status">
                <option value="not_done">Не выполнено</option>
                <option value="done_not_price">Выполнено награда не получена</option>
                <option value="done_price">Выполнено награда получена</option>
            </select><br>
            Номер повтора<input type="text" name="number_retry" value=""><br>


        </div>




        <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Создать
                </button>
            </div>
        </div>
    </form>
</div>