<?php
namespace App\Services;

use App\Models\Client;
use App\Models\Realization;
use App\Models\Task;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ClientService
{
    public static function create($request)
    {
        $task = DB::table('tasks')->where('name', $request['task'])->first();
        $is_client = Client::where('number',$request['number'])->first();
        if(empty($is_client))
        {
            $client = new Client();
            $client->number = $request['number'];
            $client->save() ? : Log::error('Ошибка сохранения пользователя');
            $client_id = $client->id;

            self::realization(true,$client_id,$task->id,$task->repeat);
        }
        else
        {
            $client_id = $is_client->id;
            self::realization(false,$client_id,$task->id,$task->repeat);
        }
    }

    public static function show()
    {
        $result = DB::table('clients')
            ->join('client_realization', 'clients.id', '=' , 'client_realization.client_id')
            ->join('tasks', 'client_realization.task_id', '=' , 'tasks.id')
            ->select('clients.id','clients.number','tasks.name','client_realization.status')
            ->groupBy('clients.id')
            ->get();
//        dd($result);
        return $result;
    }

    private static function realization($flag,$client_id,$task_id,$quantity)
    {
        $realization = new Realization();
        if($flag)
        {
            $realization->client_id = $client_id;
            $realization->task_id = $task_id;
            $realization->quantity = 1;
            if($quantity == 1)
                $realization->status = 1;
            else
                $realization->status = 0;
            $realization->save() ? : Log::error('Ошибка сохранения данных пользователя');
            }
        else
        {
            $str = Realization::where([
                'client_id' => $client_id,
                'task_id' => $task_id
            ])->first();
            if($str->quantity == $quantity)
                return 'Задание уже выполнено';
//                Log::error('Задание уже выполнено');
            ++$str->quantity;
            if($realization->quantity == $quantity)
                $str->status = 1;
            else
                $str->status = 0;
            if($str->save())
                return false;
            else
                return 'Ошибка сохранения данных выполнения';
//            $str->save() ? : Log::error('Ошибка сохранения данных выполнения');
        }
    }

    public static function bonusGift($number)
    {
        $client = Client::where('number',$number)->first();
        $task = Task::find(1);

        return self::realization(false,$client->id,1,$task->repeat);
    }






}





?>