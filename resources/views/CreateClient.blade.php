<div class="container">
    <div class="row">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/client') }}" enctype="multipart/form-data">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    {{ csrf_field() }}
                    <div class="panel-heading">Добавление клиента</div>
                    <div class="panel-body">

                        <div class="form-group{{ $errors->has('number') ? ' has-error' : '' }}">
                            <label for="number" class="col-md-4 control-label">Телефон</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="number">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('task') ? ' has-error' : '' }}">
                            <label for="task" class="col-md-4 control-label">Выберите задание</label>
                            <div class="col-md-6">
                                <select class="form-control" name="task">
                                    @foreach($tasks as $key => $value)
                                        <option value="{{ $value['name'] }}">{{ $value['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        {{--<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">--}}
                            {{--<label for="status" class="col-md-4 control-label">Выберите задание</label>--}}
                            {{--<div class="col-md-6">--}}
                                {{--<select class="form-control" name="status">--}}
                                    {{--<option value="not_done">Не выполнено</option>--}}
                                    {{--<option value="done_not_price">Выполнено награда не получена</option>--}}
                                    {{--<option value="done_price">Выполнено награда получена</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Создать
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>