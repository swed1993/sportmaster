<div class="container">
    <div class="row">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/save_category') }}" enctype="multipart/form-data">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    {{ csrf_field() }}
                    <div class="panel-heading">Изменение категории</div>
                    <div class="panel-body">
                        <input type="hidden" name="id" value="{{ $category['id'] }}">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Название категории</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" value="{{ $category['name'] }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('prize') ? ' has-error' : '' }}">
                            <label for="prize" class="col-md-4 control-label">Награждение за всю категорию</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="prize" value="{{ $category['prize'] }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Сохранить
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>