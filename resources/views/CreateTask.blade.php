<div class="container">
    <div class="row">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/task') }}" enctype="multipart/form-data">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    {{ csrf_field() }}

                    <div class="panel-heading">Категория</div>
                    <div class="panel-body">

                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Выберите категорию</label>
                            <div class="col-md-6">
                                <select class="form-control" name="category">
                                    <option></option>
                                    @foreach($categories as $key => $value)
                                        <option value="{{ $value['name'] }}">{{ $value['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('clubpro') ? ' has-error' : '' }}">
                            <label for="club_pro" class="col-md-4 control-label">Код Акции Клабпро</label>
                            <div class="col-md-6">
                                <input id="club_pro" type="text" class="form-control" name="club_pro">
                            </div>
                        </div>
                    </div>
                    <div class="panel-heading">Тип задания</div>
                    <div class="panel-body">
                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-4 control-label">Выберите тип</label>
                            <div class="col-md-6">
                                <input type="radio" name="type" value="type_all" checked>Общие задания<br>
                                <input type="radio" name="type" value="type_list">Задания со списком товара<br>
                                <input type="radio" name="type" value="type_response">Задания с ответом<br>
                                <input type="radio" name="type" value="type_scaner">Задания со сканером штрих-кода<br>
                            </div>
                            <div class="type type_list">
                                <label for="type" class="col-md-6 control-label"><textarea class="type_value text_with_file" name="type_list"></textarea></label>
                                <div class="col-md-4">
                                    <input class="add_file type_value" name="type_list_file" type="file">
                                </div>
                            </div>
                            <div class="type type_response">
                                <label for="type_response" class="col-md-4 control-label">Введите ответ</label>
                                <div class="col-md-6">
                                    <input class="type_value" name="type_response" type="text">
                                </div>
                            </div>
                            <div class="type type_scaner">
                                <label for="type_scaner_file" class="col-md-6 control-label">
                                    <textarea class="type_value text_with_file" name="type_scaner"></textarea>
                                </label>
                                <div class="col-md-4">
                                    <input class="add_file type_value" name="type_scaner_file" type="file">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-heading">Задание</div>
                    <div class="panel-body">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Название</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="name" value="">
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price" class="col-md-4 control-label">Награда</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="price" value="">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Описание</label>
                            <div class="col-md-6">
                                <textarea name="description"></textarea>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="level" class="col-md-4 control-label">Уровень</label>
                            <div class="col-md-6">
                                <select class="form-control" name="level">
                                    @foreach($levels as $key => $value)
                                        <option value="{{ $value['value'] }}">{{ $value['value'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('retry') ? ' has-error' : '' }}">
                            <label for="retry" class="col-md-4 control-label">Количество повторов</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="retry" value="1">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('retry_price') ? ' has-error' : '' }}">
                            <label for="retry_price" class="col-md-4 control-label">Награда за повтор</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="retry_price" value="">
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Создать
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>