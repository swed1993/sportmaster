<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Client;
use App\Models\Task;
use App\Services\ClientService;
use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;

class ApiSportmasterController extends Controller
{
    public function bonusGift(Request $request)
    {

        $result = ClientService::bonusGift($request->input('sender_phone'));
        $task = Task::find(1);
        if(!empty($result))
        {
            return response([
                'success' => false,
                'message' => $result
            ]);
        }
        $result = Curl::to('http://www.sportmaster.ru/rest/v1/clubpro/event/addBonuses')
            ->withData([
                'user_id' => $request->input('sender_phone'),
                'action_id' => $task->code_share
            ])
            ->post();

        if(!empty($result))
        {
            return response([
                'success' => false,
                'message' => 'Ошибка запроса в апи спортмастер'
            ]);
        }
    }

}
