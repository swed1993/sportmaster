<?php
namespace App\Http\Controllers;

use SleepingOwl\Admin\Http\Controllers\AdminController;
use SleepingOwl\Admin\Contracts\ModelConfigurationInterface;
use Illuminate\Contracts\Support\Renderable;
use AdminTemplate;

class BaseController extends AdminController
{
    private $parentBreadcrumb = 'home';

    public function render(ModelConfigurationInterface $model, $content, $title = null)
    {
        AdminDisplay::table()
            ->addStyle('test.css', asset('test.css'));
        if ($content instanceof Renderable) {
            $content = $content->render();
        }

        if (is_null($title)) {
            $title = $model->getTitle();
        }

        return AdminTemplate::view('_layout.inner')
            ->with('title', $title)
            ->with('content', $content)
//            ->with('breadcrumbKey', $this->parentBreadcrumb)
            ->with('successMessage', session('success_message'));
    }

    public function renderContent($content, $title = null)
    {
        if ($content instanceof Renderable) {
            $content = $content->render();
        }

        return view('layouts.inner')
            ->with('title', $title)
            ->with('content', $content)
//            ->with('breadcrumbKey', $this->parentBreadcrumb)
            ->with('successMessage', session('success_message'));
    }

    public function index()
    {
        return $this->renderContent('Добрый утро!','Главная');
    }
}



?>