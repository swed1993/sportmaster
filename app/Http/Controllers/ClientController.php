<?php
namespace App\Http\Controllers;

use App\Models\Task;
use App\Services\ClientService;
use Illuminate\Http\Request;
class ClientController extends BaseController
{
    public function create_client(Request $request)
    {
        return $this->renderContent(view('CreateClient')->with('tasks',Task::all()));
    }

    public function list_clients(Request $request)
    {
//        $arr = ClientService::show();
        return $this->renderContent(view('ShowClients')->with('result',ClientService::show()));
    }

    public function client(Request $request)
    {
        ClientService::create($request->all());
        return redirect('/');
    }
}


?>