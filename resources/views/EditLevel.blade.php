<div class="container">
    <div class="row">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/save_level') }}" enctype="multipart/form-data">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    {{ csrf_field() }}
                    <div class="panel-heading">Изменение уровня</div>
                    <div class="panel-body">


                        <input type="hidden" name="id" value="{{ $level['id'] }}">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="value" class="col-md-4 control-label">Название уровня</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="value" value="{{ $level['value'] }}">
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Сохранить
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>