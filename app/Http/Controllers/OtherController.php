<?php
namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Level;
use App\Services\NewTask;
use App\Services\OtherService;
use App\Services\ValidateTask;
use Illuminate\Http\Request;
use SleepingOwl\Admin\Http\Controllers\AdminController;
use SleepingOwl\Admin\Contracts\ModelConfigurationInterface;
use Illuminate\Contracts\Support\Renderable;
use AdminTemplate;

class OtherController extends BaseController
{

    
    public function category()
    {
        return $this->renderContent(view('CreateCategory'));
    }

    public function create_category(Request $request)
    {
        OtherService::create_category($request->all());
        return redirect('/');
    }

    public function level()
    {
        return $this->renderContent(view('CreateLevel'));
    }

    public function create_level(Request $request)
    {
        OtherService::create_level($request->all());
        return redirect('/');
    }

    public function status()
    {
        return $this->renderContent(view('CreateStatus'));
    }

    public function create_status(Request $request)
    {
        dd($request->all());
        $rr = OtherService::create_status($request->all());
    }

    public function list_category()
    {
        return $this->renderContent(view('ListCategories')->with('result',Category::all()->toArray()));
    }
    public function category_detail(Category $category)
    {
        return $this->renderContent(view('EditCategory')->with('category',$category));
    }
    public function save_category(Request $request)
    {
        $category = Category::find($request['id']);
        $category->name = $request['name'];
        $category->prize = $request['prize'];
        $category->save();
        return redirect('/list_category');
    }

    public function category_destroy(Category $category)
    {
        $category->delete();
        return redirect('/');
    }

    public function list_levels()
    {
        return $this->renderContent(view('ListLevels')->with('result',Level::all()->toArray()));
    }
    
    public function level_detail(Level $level)
    {
        return $this->renderContent(view('EditLevel')->with('level',$level));
    }

    public function save_level(Request $request)
    {
        $level = Level::find($request['id']);
        $level->value = $request['value'];
        $level->save();
        return redirect('/list_levels');
    }

    public function level_destroy(Level $level)
    {
        $level->delete();
        return redirect('/list_levels');
    }

}



?>