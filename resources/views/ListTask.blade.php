<div class="col-md-12">
    <table class="table table-hover">
        <thead>
        <tr class="info">
            <td><h4>Категория</h4></td>
            <td><h4>Задание</h4></td>
            <td><h4>Порядковый номер</h4></td>
            <td></td>
            <td></td>
        </tr>
        </thead>
        <tbody>
        @foreach($result as $key => $val)
            <tr class="success">
                <td>{{ $val['categories']['name'] }}</td>
                <td>{{ $val['name'] }}</td>
                <td>{{ $val['serial_number']['number'] }}</td>
                <td>
                    <form action="{{ url('task/'.$val['id']) }}" method="POST">
                        {!! csrf_field() !!}
                        <button type="submit" class="btn btn-info">
                            <i class="fa fa-edit"></i> Редактировать
                        </button>
                    </form>
                </td>
                <td>
                    <form action="{{ url('task/'.$val['id']) }}" method="POST">
                        {!! csrf_field() !!}
                        {!! method_field('DELETE') !!}

                        <input type="hidden" name="_method" value="DELETE">
                        <button type="submit" class="btn btn-danger">
                            <i class="fa fa-trash"></i> Удалить
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>