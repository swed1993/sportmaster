<?php
namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Level;
use App\Models\Parameter;
use App\Models\Task;
use App\Services\NewTask;
use App\Services\ValidateTask;
use Illuminate\Http\Request;
use SleepingOwl\Admin\Http\Controllers\AdminController;
use SleepingOwl\Admin\Contracts\ModelConfigurationInterface;
use Illuminate\Contracts\Support\Renderable;
use AdminTemplate;

class TaskController extends BaseController
{

    public function index()
    {
        return $this->renderContent(view('CreateTask')->with([
            'categories' => Category::all(),
            'levels' => Level::all(),
        ]));

    }

    public function task(Request $request)
    {
        $request = ValidateTask::base_check($request->all());
        NewTask::create($request);
        return redirect('/');
    }

    public function list_task()
    {
        return $this->renderContent(view('ListTask')->with('result',NewTask::list_tasks()));
    }

    public function destroy(Task $task)
    {
        $task->delete();
        return redirect('/');
    }

    public function task_detail(Task $task)
    {
        $result = [
            'categories' => Category::all()->toArray(),
            'levels' => Level::all()->toArray(),
            'task' => $task->toArray(),
            'cat_check' => $task->categories()->getResults()->toArray(),
            'number' => $task->serial_number()->getResults()->toArray(),
            'level' => $task->levels()->getResults()->toArray(),
            'parameters' => Parameter::where('task_id',$task->id)->get()->toArray(),

        ];
        return $this->renderContent(view('EditTask')->with($result));
    }

    public function edit_task(Request $request)
    {
        ValidateTask::editTask($request->all());
        return redirect('/');
    }



}



?>