<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Realization extends Model
{
    protected $table = 'client_realization';
    public $timestamps = false;

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function task()
    {
        return $this->belongsTo(Task::class);
    }
}
?>