<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::auth();

$api = app('Dingo\Api\Routing\Router');

$api->version('v1',[ 'namespace'=>'App\Http\Controllers\Api'], function ($api) {
    $api->get('ff','ApiMobileController@index');
//    $api->get('/ff',function (){
//        return response()->json([
//            'test_key' => 'test_value',
//            'access_token' => \Illuminate\Http\Request::header('access_token'),
//        ]);
//    });


//    $api->group(['middleware' => 'api.mobile'], function ($api) {
//        // Endpoints registered here will have the "foo" middleware applied.
//    });
});

$api->version('v1',[ 'namespace'=>'App\Http\Controllers\Api','middleware' => ['api.sportmaster']], function ($api) {
    $api->post('bonusGift','ApiSportmasterController@bonusGift');

});

Route::group(['middleware' => ['auth']], function (){
//    Route::get('/', function () {
//        return view('welcome');
//    });
//
//
//
//    Route::get('/home', 'HomeController@index');
});
