$(document).ready(function () {
    $("input[name=type]:radio").change(function(){
        $(".type_value").val('');
        $("#txt_for_edit").hide();
        var name = $(this).val();
        if(name == 'type_all')
            $('.type').hide();
        else
        {
            $('.type').hide();
            $("." + name).show();
        }

    });
    
    $(".add_file").click(function () {
        $(".type_value").val('');
    });
    
    $("#select_cat").click(function(){
        $("#new_cat").val('');
        $("#cat_prize").val('');
    });
    $("#create_cat").click(function(){
        $("#select_cat select :nth-child(1)").attr("selected", "selected");
    });

    $(".text_with_file").click(function () {
        $(".text_with_file").siblings("input").val('');
    });
    $('.box_cat').click(function () {
        $('#box_cat').toggle();
    });
    $('.box_type').click(function () {
        $('#box_type').toggle();
    });
    $('.box_task').click(function () {
        $('#box_task').toggle();
    });
});
//# sourceMappingURL=all.js.map
