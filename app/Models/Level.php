<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'levels';
    public $timestamps = false;


    protected $fillable = [
        'id',
        'value',
    ];

    public function tasks()
    {
        return $this->hasMany(Task::class);
    }
}
?>