<!DOCTYPE html>
<html lang="en">
<head>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
{!!
    Meta::setTitle(AdminTemplate::makeTitle($title))
        ->addMeta(['charset' => 'utf-8'], 'meta::charset')
        ->addMeta(['content' => csrf_token(), 'name' => 'csrf-token'])
        ->addMeta(['content' => 'width=device-width, initial-scale=1', 'name' => 'viewport'])
        ->addMeta(['content' => 'IE=edge', 'http-equiv' => 'X-UA-Compatible'])
        ->addCssElixir()
        ->addJsElixir('js/all.js')
        ->render()
!!}

{{--    <link href="{{ elixir('css/all.css') }}" rel="stylesheet">--}}


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    @stack('scripts')
</head>
<body class="skin-blue sidebar-mini">
@yield('content')
</body>
</html>