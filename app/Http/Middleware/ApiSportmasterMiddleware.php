<?php

namespace App\Http\Middleware;

use Closure;

class ApiSportmasterMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->header('Authorization') != env(API_KEY_BACKEND))
            return response('Failed Authorization', 401);
        return $next($request);
    }
}
