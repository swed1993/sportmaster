<div class="col-md-12">
    <blockquote>Статус задания: 0 - не выполнено,1 - выполнено,награда не выплачена, 2 - выполнено</blockquote>
    <table class="table table-hover">
        <thead>
        <tr class="info">
            <td><h4>ID</h4></td>
            <td><h4>Номер телефона</h4></td>
            <td><h4>Название задание</h4></td>
            <td><h4>Статус</h4></td>
        </tr>
        </thead>
        <tbody>
        @foreach($result as $key => $val)
            <tr class="success">
                <td>{{ $val->id }}</td>
                <td>{{ $val->number }}</td>
                <td>{{ $val->name }}</td>
                <td>{{ $val->status }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>