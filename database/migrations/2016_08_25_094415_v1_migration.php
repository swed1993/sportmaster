<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class V1Migration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('type_tasks', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('categories', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->integer('prize');
        });


        Schema::create('clients', function (Blueprint $table){
            $table->increments('id');
            $table->string('number');
        });

        Schema::create('levels', function (Blueprint $table){
            $table->increments('id');
            $table->string('value');
        });

        Schema::create('tasks', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->integer('price');
            $table->text('description');
            $table->integer('level_id')->unsigned();
            $table->integer('repeat')->default(1);
            $table->integer('repeat_prize')->default(NULL);
            $table->string('type_task');
            $table->integer('category_id')->unsigned();
            $table->string('code_share');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('level_id')->references('id')->on('levels')->onDelete('cascade');

        });

        Schema::create('parameters_tasks', function (Blueprint $table){
            $table->increments('id');
            $table->integer('task_id')->unsigned();
            $table->string('value');
            $table->index('task_id');

            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
        });

        Schema::create('serial_number', function (Blueprint $table){
            $table->integer('category_id')->unsigned();
            $table->integer('task_id')->unsigned();
            $table->integer('number');

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
        });

        Schema::create('client_realization', function (Blueprint $table){
            $table->increments('id');
            $table->integer('client_id')->unsigned();
            $table->integer('task_id')->unsigned();
            $table->integer('quantity');
            $table->integer('status');

            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');
        });







    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
