<div class="container">
    <div class="row">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/edit_task') }}" enctype="multipart/form-data">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    {{ csrf_field() }}
                    <input type="hidden" name="task_id" value="{{ $task['id']}}">
                    <input type="hidden" name="type_task_old" value="{{ $task['type_task']}}">
                    <div class="panel-heading">Категория</div>
                    <div class="panel-body">

                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Выберите категорию</label>
                            <div class="col-md-6">
                                <select class="form-control" name="category">
                                    <option></option>
                                    @foreach($categories as $key => $value)
                                        @if($value['id'] == $cat_check['id'])
                                            <option value="{{ $value['name'] }}" selected>{{ $value['name'] }}</option>
                                        @else
                                            <option value="{{ $value['name'] }}">{{ $value['name'] }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('clubpro') ? ' has-error' : '' }}">
                            <label for="club_pro" class="col-md-4 control-label">Код Акции Клабпро</label>
                            <div class="col-md-6">
                                <input id="club_pro" type="text" class="form-control" name="club_pro" value="{{ $task['code_share'] }}">
                            </div>
                        </div>
                    </div>
                    <div class="panel-heading">Тип задания</div>
                    <div class="panel-body">
                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-4 control-label">Выберите тип</label>
                            @if($task['type_task'] == 'type_all')
                                <div class="col-md-6">
                                <input type="radio" name="type" value="type_all" checked>Общие задания<br>
                                <input type="radio" name="type" value="type_list">Задания со списком товара<br>
                                <input type="radio" name="type" value="type_response">Задания с ответом<br>
                                <input type="radio" name="type" value="type_scaner">Задания со сканером штрих-кода<br>
                                </div>
                            @elseif($task['type_task'] == 'type_list')
                                <div class="col-md-6">
                                <input type="radio" name="type" value="type_all">Общие задания<br>
                                <input type="radio" name="type" value="type_list" checked>Задания со списком товара<br>
                                <input type="radio" name="type" value="type_response">Задания с ответом<br>
                                <input type="radio" name="type" value="type_scaner">Задания со сканером штрих-кода<br>
                                </div>
                            @elseif($task['type_task'] == 'type_response')
                                <div class="col-md-6">
                                <input type="radio" name="type" value="type_all">Общие задания<br>
                                <input type="radio" name="type" value="type_list">Задания со списком товара<br>
                                <input type="radio" name="type" value="type_response" checked>Задания с ответом<br>
                                <input type="radio" name="type" value="type_scaner">Задания со сканером штрих-кода<br>
                                </div>

                            @elseif($task['type_task'] == 'type_scaner')
                                <div class="col-md-6">
                                <input type="radio" name="type" value="type_all">Общие задания<br>
                                <input type="radio" name="type" value="type_list">Задания со списком товара<br>
                                <input type="radio" name="type" value="type_response">Задания с ответом<br>
                                <input type="radio" name="type" value="type_scaner" checked>Задания со сканером штрих-кода<br>
                                </div>

                            @endif

                            <div>
                                <label for="type" class="col-md-6 control-label">
                                                    <textarea name="type_edit">
                                                        @foreach($parameters as $key => $val)
                                                            {{ $val['value'] }}
                                                        @endforeach
                                                    </textarea>
                                </label>
                                <div class="col-md-4">
                                    <input name="edit_list_file" type="file">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-heading">Задание</div>
                    <div class="panel-body">
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Название</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="name" value="{{ $task['name'] }}">
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                            <label for="price" class="col-md-4 control-label">Награда</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="price" value="{{ $task['price'] }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Описание</label>
                            <div class="col-md-6">
                                <textarea name="description">{{ $task['description'] }}</textarea>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="level" class="col-md-4 control-label">Уровень</label>
                            <div class="col-md-6">
                                <select class="form-control" name="level">
                                    @foreach($levels as $key => $value)
                                        @if($value['id'] == $task['level_id'])
                                            <option value="{{ $value['value'] }}" selected>{{ $value['value'] }}</option>
                                        @else
                                            <option value="{{ $value['value'] }}">{{ $value['value'] }}</option>
                                        @endif
                                    @endforeach

                                </select>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('retry') ? ' has-error' : '' }}">
                            <label for="retry" class="col-md-4 control-label">Количество повторов</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="retry" value="{{ $task['repeat'] }}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('retry_price') ? ' has-error' : '' }}">
                            <label for="retry_price" class="col-md-4 control-label">Награда за повтор</label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="retry_price" value="{{ $task['repeat_prize'] }}">
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn btn-primary">
                                Сохранить
                            </button>
                            <button type="submit" class="btn btn-danger">
                                <a href="{{ url('/list_task') }}">Отмена</a>
                            </button>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>