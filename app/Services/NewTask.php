<?php
namespace App\Services;


use App\Models\Category;
use App\Models\Parameter;
use App\Models\SerialNumber;
use App\Models\Task;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class NewTask
{
    public static function create($request)
    {
        $id = self::saveTask($request);
        if(empty($request['list_value']))
            return;
        self::parseParameters($request['list_value'],$id);
    }

    private static function parseParameters($list,$id)
    {
        if(is_array($list))
            self::saveParameters($list,$id);
        elseif(is_string($list))
        {
            $arr = explode(';',$list);
            self::saveParameters($arr,$id);
        }
        else
        {
            Log::error('Неверный формат данных');
            redirect('/create_task');
        }
    }



    private static function saveTask($request)
    {

        $request['category'] = Category::where('name',$request['category'])->first()['id'];

        $model = new Task();

        $model->name = $request['name'];
        $model->price = $request['price'];
        $model->description = $request['description'];
        $model->level_id = $request['level'];
        $model->repeat = $request['retry'];
        $model->repeat_prize = $request['retry_price'];
        $model->type_task = $request['type'];
        $model->category_id = $request['category'];
        $model->code_share = $request['club_pro'];

        if(!$model->save())
        {
            Log::error('Ошибка создания задания');
            redirect('/create_task');
        }

        $length = SerialNumber::where('category_id',$request['category'])
            ->count();
        $serial_number = new SerialNumber();
        $serial_number->category_id = $request['category'];
        $serial_number->task_id = $model->id;
        $serial_number->number = $length + 1;

        if($serial_number->save())
            return $model->id;
        else
        {
            Log::error('Ошибка создания порядкового номера');
            redirect('/create_task');
        }

    }

    public static function saveParameters($arr,$id)
    {
        foreach ($arr as $value)
        {
            $model = new Parameter();
            $model->task_id = $id;
            $model->value = $value;
            $model->save() ? : Log::error('Ошибка сохранения параметров задания');
        }
    }

    public static function list_tasks()
    {
        $result = Task::with('categories','serial_number')->get();
        return $result->toArray();
    }

    public static function task_detail($task)
    {
//        dd($task);
//        $result ['']
//        $tt = DB::table('')
    }

    
}
?>