<?php
namespace App\Services;

use App\Models\Category;
use App\Models\Level;
use App\Models\Parameter;
use App\Models\Task;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Parsers\ExcelParser;

class ValidateTask
{
    private static $type = [
        'type_all',
        'type_list',
        'type_response',
        'type_scaner'
    ];


    public static function base_check($request)
    {
        $request = self::type($request);
        $request = self::getLevelId($request);
        return $request;

    }

    private static function type($request)
    {
        $key = $request['type'];
        foreach (self::$type as $value)
        {
            if($key == $value)
                continue;
            else
                unset($request[$value]);
        }
        if($key == 'type_all')
            $request[$key] = '';
        $request['list_value'] = $request[$key];
        unset($request[$key]);
        if($key == 'type_list' || $key == 'type_scaner')
        {
            if(isset($request['type_list_file']))
            {
                $request['list_value'] = self::excelReader($request['type_list_file']);
                unset($request['type_list_file']);
            }
            if(isset($request['type_scaner_file']))
            {
                $request['list_value'] = self::excelReader($request['type_scaner_file']);
                unset($request['type_scaner_file']);
            }
        }
        return $request;
    }

    private static function excelReader($file)
    {
        $arr = [];
        $result = Excel::load($file,function($reader){
            $results = $reader->all();
        })->get();
        foreach($result as $value)
        {
            foreach($value as $val)
            {
                $arr[] = $val;
            }
        }
        return $arr;
    }
    private static function getLevelId($request)
    {
        $level = Level::where('value',$request['level'])->first();
        $request['level'] = $level->id;
        return $request;

    }
    
    public static function editTask($request)
    {
        $request = self::getLevelId($request);
        $category = Category::where('name',$request['category'])->first();
        $file = self::excelReader($request['edit_list_file']);
        $text = self::normalizationList($request['type_edit']);


        $task = Task::find($request['task_id']);

        $task->name = $request['name'];
        $task->price = $request['price'];
        $task->description = $request['description'];
        $task->level_id = $request['level'];
        $task->repeat = $request['retry'];
        $task->repeat_prize = $request['retry_price'];
        $task->type_task = $request['type'];
        $task->category_id = $category->id;
        $task->code_share = $request['club_pro'];
        $task->save();

        Parameter::where('task_id',$request['task_id'])->delete();
        NewTask::saveParameters($file,$request['task_id']);
        NewTask::saveParameters($text,$request['task_id']);

    }

    private static function normalizationList($list)
    {
        $str = strtr($list, [' ' => '']);
        $result = preg_split('/\n|\r\n?/', $str);
        return $result;
    }
}
?>