<?php

use SleepingOwl\Admin\Navigation\Page;


return [


    [
        'title' => 'Выход',
        'url'   => route('logout'),
    ],
    [
        'title' => 'Уровни',
        'pages' => [
            [
                'title' => 'Создать уровень',
                'url'   => route('level'),
            ],
            [
                'title' => 'Список уровней',
                'url'   => route('list_levels'),
            ],

        ]
    ],
    [
        'title' => 'Категории',
        'pages' => [
            [
                'title' => 'Создать категорию',
                'url'   => route('category'),
            ],
            [
                'title' => 'Список категорий',
                'url'   => route('list_category'),
            ],

        ]
    ],
    [
        'title' => 'Пользователи',
        'pages' => [
            [
                'title' => 'Добавить пользователя',
                'url'   => route('create_client'),
            ],
            [
                'title' => 'Просмотр пользователей',
                'url'   => route('list_clients'),
            ],

        ]
    ],
    [
        'title' => 'Задания',
        'pages' => [
            [
                'title' => 'Добавить задание',
//                'label' => 'Добавить задание',
                'url'   => route('create_task'),
            ],
            [
                'title' => 'Редактировать задания',
                'url'   => route('list_task'),
            ],

        ]
    ],
//    [
//        'title' => 'Главная',
//        'url'   => route('dashboard'),
//    ],


];